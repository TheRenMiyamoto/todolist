package com.example.todolist.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.todolist.model.TodoItem;

public interface TodoRepo extends JpaRepository<TodoItem, Long> {

}
