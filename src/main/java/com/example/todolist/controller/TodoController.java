package com.example.todolist.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.todolist.model.TodoItem;
import com.example.todolist.repo.TodoRepo;

@RestController
@RequestMapping(value = "/todo")
public class TodoController {

	@Autowired
	private TodoRepo todoRepo;

	@GetMapping
	public List<TodoItem> findAll() {
		return todoRepo.findAll();
	}

	@PostMapping
	public TodoItem save(@NonNull @RequestBody TodoItem todoItem) {
		return todoRepo.save(todoItem);
	}

}
